<?php

namespace Drupal\news\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity;
use Drupal\Core\Cache\UncacheableDependencyTrait;


/**
 * Provides a 'NewsBlock3' Block.
 *
 * @Block(
 *   id = "NewsBlock3",
 *   admin_label = @Translation("NewsBlock3 block"),
 *   category = @Translation("NewsBlock3"),
 * )
 */
class NewsBlock3 extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
	$num=2;
	$out=news_get_node_summary($num);
    return [
      '#markup' => $out,
    ];
  }
  public function getCacheMaxAge()
    {
        return 0;
    }

}

