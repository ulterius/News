<?php

namespace Drupal\news\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity;
use Drupal\Core\Cache\UncacheableDependencyTrait;


/**
 * Provides a 'NewsBlock2' Block.
 *
 * @Block(
 *   id = "NewsBlock2",
 *   admin_label = @Translation("NewsBlock2 block"),
 *   category = @Translation("NewsBlock2"),
 * )
 */
class NewsBlock2 extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
	$num=1;
	$out=news_get_node_summary($num);
    return [
      '#markup' => $out,
    ];
  }
  public function getCacheMaxAge()
    {
        return 0;
    }

}

