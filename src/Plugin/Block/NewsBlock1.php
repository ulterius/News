<?php

namespace Drupal\news\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Entity;
use Drupal\Core\Cache\UncacheableDependencyTrait;


/**
 * Provides a 'NewsBlock1' Block.
 *
 * @Block(
 *   id = "NewsBlock1",
 *   admin_label = @Translation("NewsBlock1 block"),
 *   category = @Translation("NewsBlock1"),
 * )
 */
class NewsBlock1 extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
	$num=0;
	$out=news_get_node_summary($num);
    return [
      '#markup' => $out,
    ];
  }
  public function getCacheMaxAge()
    {
        return 0;
    }
  

}

