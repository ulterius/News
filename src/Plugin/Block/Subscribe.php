<?php

namespace Drupal\news\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides a 'Hello' Block.
 *
 * @Block(
 *   id = "Subscribe",
 *   admin_label = @Translation("Subscribe block"),
 *   category = @Translation("Subscribe"),
 * )
 */
class Subscribe extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $out[] = [ '#markup' => t('Subscribe to the newsletters') ];
    $out[] = \Drupal::formBuilder()->getForm('\Drupal\news\Form\Subscribe');
    $out[]= [
                '#attached' => [
                        'library' => [
                        'news/news',
                        ]
                ]
        ];
    return $out;
  }
  public function getCacheMaxAge()
    {
        return 0;
    }

}

