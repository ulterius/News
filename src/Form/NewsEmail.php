<?php

/**
 * @file
 * Contains \Drupal\news\Form\NewsEmail.
 */

namespace Drupal\news\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Entity;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Symfony\Component\HttpFoundation\RedirectResponse;

class NewsEmail extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'news_email';
  }

  public function buildForm(array $form, FormStateInterface $form_state, $nid='') {
    $result = \Drupal::database()->query("SELECT email FROM {newsemail_current");
    foreach ($result as $row) {
	    $emails.=$row->email.', ';
    }
    $form['nid'] = ['#type' => 'value', '#value' => $nid];
    $content = \Drupal\node\Entity\Node::load($nid);
    $msg = t('Send this news?: ').$content->title->value.'<br>'.t('to').': '.$emails.'<br>';
    $form['submit'] = [
      '#prefix' => $msg,
      '#type' => 'submit',
      '#value' => t('Are you sure?'),
    ];
    $form['cancel'] = [
      '#type' => 'submit',
      '#value' => $this->t('Cancel'),
      '#submit' => ['::cancel'],
    ];

    return $form;
  }

public function cancel(array $form, FormStateInterface $form_state) {
      $form_state->setRedirect('news.news');
  }


  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    global $base_url;
     $form_state->setRedirect('news.news');
	$nid = $form_state->getValue(['nid']);
	$node = \Drupal\node\Entity\Node::load($nid);
      $rows = [];
      $from = \Drupal::config('news.settings')->get('news_emails_receive');
      //header part
      $rows[] = [
        [
          'data' => Markup::create(\Drupal::config('news.settings')->get('news_header')),
          'class' => 'header',
        ]
        ];
      //Body
	$title=Link::fromTextAndUrl($node->title->value,Url::fromUri(news_create_url_from_nid($nid)))->toString();
      $body='<center><H3>'.$title.'</H3></center><p></br>';
      $body.=$node->body->value;
      $rows[] = [
        [
          'data' => Markup::create($body),
          'class' => 'body',
        ]
        ];
      $result = \Drupal::database()->query("SELECT email,code FROM {newsemail_current");
      foreach ($result as $row) {
        //footer part
	$unsubscribe =Link::fromTextAndUrl(t('Unsubscribe'), Url::fromUri($base_url.'/news/unsubscribe/'.$row->code))->toString();
        $rows[] = [
          [
            'data' => Markup::create(\Drupal::config('news.settings')->get('news_signature') . $unsubscribe),
            'class' => 'footer',
          ]
          ];
        //Build de message.
	$table = [
                '#type' => 'table',
                '#header' => array(),
                '#rows' => $rows,
        ];
        $msg .= \Drupal::service('renderer')->render($table);
        //Send it.
	$subject='['.\Drupal::config('system.site')->get('name').'] '.$node->title->value;
        $result=news_send_html_mail($msg, $subject,$row->email, $from,$cc='',$bcc='');
        //save mail addresses send.
	$emails .= $row->email . " ";
      }
	$entry['time'] = time();
      $entry['node'] = $nid;
      $entry['subject'] = $node->title->value;
      $entry['emails'] = $emails;
      try {
        $id = \Drupal::database()->insert('newsemail_send')->fields($entry)->execute();
      }

        catch (Exception $e) {
        \Drupal::messenger()->addError(t('db_insert failed. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]));
      }

      \Drupal::messenger()->addMessage($node->title->value . t("emails has been send"));
//      return $this->redirect('news.news');
//      return new RedirectResponse(Url::fromRoute('news.news')->setAbsolute()->toString());
  }

}
?>
