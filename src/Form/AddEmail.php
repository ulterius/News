<?php

/**
 * @file
 * Contains \Drupal\news\Form\NewsEmail.
 */

namespace Drupal\news\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class NewsEmail extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'news_email';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $node = NULL) {
    $form = array();
    $form['email'] = array('#type' => 'textfield', '#title' => t('Add email address'), '#size' => 20,);
    $form['submit'] = array('#type' => 'submit', '#value' => t('Add'));
    return $form;
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $email = trim($form_state['values']['email']);
    $row = \Drupal::database()->query("SELECT email FROM {newsemail_current} where email=:email", array(':email' => $email))->fetchObject();
    if ($row) {
        \Drupal::messenger()->addError($email . t(' allready exists!'));
    } else {
        $entry['time'] = time();
        $entry['email'] = $email;
        $entry['code'] = md5($email . $time);;
        try {
            $id = \Drupal::database()->insert('newsemail_current')->fields($entry)->execute();
        }
        catch(Exception $e) {
            \Drupal::messenger()->addError(t('db_insert failed. Message = %message, query= %query', array('%message' => $e->getMessage(), '%query' => $e->query_string)));
            return;
        }
    }
    return;
  }

}

?>
