<?php

/**
 * @file
 * Contains \Drupal\news\Form\SubscribeManual.
 */

namespace Drupal\news\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\Core\Link;


class SubscribeManual extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'news_email';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $node = NULL) {
    $form = array();
    $form['email'] = array(
			'#type' => 'textfield',
			'#description' => t('Subscribe direct without confirmation'),
			'#size' => 30,
			);
    $form['submit'] = array(
			'#type' => 'submit',
			 '#value' => t('Send'),
			);
    $form['#theme'] = 'subscribenews';
    return $form;
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $user = \Drupal::currentUser();
    global $base_url;
    $email = trim($form_state->getValue(['email']));
    if (\Drupal::service('email.validator')->isValid($email)) {
        $row = \Drupal::database()->query("SELECT email FROM {newsemail_current} where email=:email;", array(':email' => $email))->fetchObject();
        if ($row) {
            \Drupal::messenger()->addError($email . t(' allready exists!'));
        } else {
        	$entry['time'] = time();
        	$entry['email'] = $email;
        	$entry['code'] = md5($email . $time);;
		try {
       		 	$id = \Drupal::database()->insert('newsemail_current')->fields($entry)->execute();
      		}
        	catch (Exception $e) {
        		\Drupal::messenger()->addError(t('db_insert failed. Message = %message, query= %query', [
          		'%message' => $e->getMessage(),
          		'%query' => $e->query_string,
        		]));
        		return $this->redirect('<front>');
		}
      	}
    } else {
        \Drupal::messenger()->addError(t('Please enter a valid email address'));
    }
    return;
  }
}

?>
