<?php

/**
 * @file
 * Contains \Drupal\news\Form\NewsAdminSettings.
 */

namespace Drupal\news\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;

class NewsAdminSettings extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'news_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('news.settings');
/*
    \Drupal::configFactory()->getEditable('news.settings')->set('news_signature', $form_state['values']['news_signature']['value'])->save();
*/
/*
    $config->set('news_header', $form_state->getValue('news_header'));
    $config->set('news_subscribe', $form_state->getValue('news_subscribe'));
    $config->set('news_welcome', $form_state->getValue('news_welcome'));
*/
    $config->set('news_signature', $form_state->getValue(['news_signature','value']));
    $config->set('news_header', $form_state->getValue(['news_header','value']));
    $config->set('news_subscribe', $form_state->getValue(['news_subscribe','value']));
    $config->set('news_welcome', $form_state->getValue(['news_welcome','value']));
    $config->set('news_mail_css', $form_state->getValue(['news_mail_css']));
    $config->set('news_emails_receive', $form_state->getValue('news_emails_receive'));
    $config->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['news.settings'];
  }


  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $form = [];
    $form['news_header'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => t('Edit mail header'),
      '#default_value' => \Drupal::config('news.settings')->get('news_header'),
    ];
    $form['news_signature'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => t('Edit mail footer'),
      '#default_value' => \Drupal::config('news.settings')->get('news_signature'),
    ];
    $form['news_subscribe'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => t('Subscribe text'),
      '#default_value' => \Drupal::config('news.settings')->get('news_subscribe'),
    ];
    $form['news_welcome'] = [
      '#type' => 'text_format',
      '#format' => 'full_html',
      '#title' => t('Welcome subscribe text'),
      '#default_value' => \Drupal::config('news.settings')->get('news_welcome'),
    ];
    $form['news_mail_css'] = [
      '#type' => 'textarea',
      '#rows' => 40,

      '#title' => t('Stylesheet html mail'),
      '#default_value' => \Drupal::config('news.settings')->get('news_mail_css'),
    ];
    $form['news_emails_receive'] = [
      '#type' => 'textfield',
      '#title' => t('Email address used for news'),
      '#default_value' => \Drupal::config('news.settings')->get('news_emails_receive'),
    ];
    return parent::buildForm($form, $form_state);
  }

}
?>
