<?php

/**
 * @file
 * Contains \Drupal\news\Form\Subscribe.
 */

namespace Drupal\news\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Markup;
use Drupal\Core\Url;
use Drupal\Core\Link;


class Subscribe extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'news_email';
  }

  public function buildForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state, $node = NULL) {
    $form = array();
    $form['email'] = array(
			'#type' => 'textfield',
			'#description' => t('Latest news email subscription'),
			'#size' => 30,
			);
    $form['submit'] = array(
			'#type' => 'submit',
			 '#value' => t('Send'),
			);
    $form['#theme'] = 'subscribenews';
    return $form;
  }

  public function submitForm(array &$form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $user = \Drupal::currentUser();
    global $base_url;
    $email = trim($form_state->getValue(['email']));
    if (\Drupal::service('email.validator')->isValid($email)) {
        $row = \Drupal::database()->query("SELECT email FROM {newsemail_current} where email=:email;", array(':email' => $email))->fetchObject();
        if ($row) {
            \Drupal::messenger()->addError($email . t(' allready exists!'));
        } else {
        $entry['time'] = time();
        $entry['email'] = $email;
        $entry['code'] = md5($email . $time);;
            try {
                $id = \Drupal::database()->insert('newsemail_new')->fields($entry)->execute();
            }
            catch(Exception $e) {
                \Drupal::messenger()->addError(t('db_insert failed. Message = %message, query= %query', array('%message' => $e->getMessage(), '%query' => $e->query_string)));
                return;
            }

//Setup mail
	$to = $email;
	$from = \Drupal::config('news.settings')->get('news_emails_receive');
//subject
	$subject='['.\Drupal::config('system.site')->get('name').'] '.t("Subscription confirmation ");
	$url=$base_url.'/news/subscribe/'.$entry['code'];
	$link=Link::fromTextAndUrl($url, Url::fromUri($url))->toString();
	$header=Markup::create(\Drupal::config('news.settings')->get('news_header'));
	$body=Markup::create(\Drupal::config('news.settings')->get('news_subscribe').'<p><br>'.$link);
	$footer=Markup::create(\Drupal::config('news.settings')->get('news_signature'));
	//Header
	$rows[] = [
		[
		'data' => $header,
		'class' => 'header',
		]
	];
	//Body
	$rows[] = [
		[
		'data' => $body,
		'class' => 'body',
		]
	];
	//Footer
	$rows[] = [
		[
		'data' => $footer,
		'class' => 'footer',
		]
	];
	$table = [
		'#type' => 'table',
		'#header' => array(),
		'#rows' => $rows,
 	];
	$msg .= \Drupal::service('renderer')->render($table);	
	$result=news_send_html_mail($msg, $subject,$to, $from,$cc='',$bcc='');
	if (!empty($result)) {
   		\Drupal::messenger()->addError(t('There was a problem sending your message and it was not sent.').$result);
 	}
 	else {
		\Drupal::messenger()->addMessage(t('Confirmation mail is send to ') . $email . t(' please click on the link in the mail to confirm your subscription.'));
 	}

        }
    } else {
        \Drupal::messenger()->addError(t('Please enter a valid email address'));
    }
    return;
  }
}

?>
