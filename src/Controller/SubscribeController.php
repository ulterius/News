<?php /**
 * @file
 * Contains \Drupal\news\Controller\DefaultController.
 */

namespace Drupal\news\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Datetime;
use Drupal\Core\Entity;
use Drupal\Core\Render\Element; 
use Drupal\Core\Render\Markup;

/**
 * Default controller for the news module.
 */
class SubscribeController extends ControllerBase {

  public function news_subscribe($code) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    global $base_url;
    $row = \Drupal::database()->query("SELECT email FROM {newsemail_new} where code=:code;", [
      ':code' => $code
      ])->fetchObject();
    if ($row->email) {
      $email = $row->email;
      $entry['time'] = time();
      $entry['email'] = $email;
      $entry['code'] = $code;
      try {
        $id = \Drupal::database()->insert('newsemail_current')->fields($entry)->execute();
      }
      
        catch (Exception $e) {
        \Drupal::messenger()->addError(t('db_insert failed. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]));
        return $this->redirect('<front>');
      }
      //Remove from new subscribed table
      try {
        \Drupal::database()->delete('newsemail_new')->condition('code', $code)->execute();
      }
      
        catch (Exception $e) {
        \Drupal::messenger()->addError(t('db_delete failed. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]));
        return $this->redirect('<front>');
      }
	$to = $email;
        $subject='['.\Drupal::config('system.site')->get('name').'] '.t("Subscription confirmation ");
      $from = \Drupal::config('news.settings')->get('news_emails_receive');
      $header=Markup::create(\Drupal::config('news.settings')->get('news_header'));
      $body = Markup::create(\Drupal::config('news.settings')->get('news_welcome'));
      $unsubscribe =Link::fromTextAndUrl(t('Unsubscribe'), Url::fromUri($base_url.'/news/unsubscribe/'.$entry['code']))->toString();
      $footer=Markup::create(\Drupal::config('news.settings')->get('news_signature').$unsubscribe);
        //Header
        $rows[] = [
                [
                'data' => $header,
                'class' => 'header',
                ]
        ];
	//Body
      $rows[] = [
        [
          'data' => $body,
          'class' => 'body',
        ]
        ];
      //footer part
      $rows[] = [
        [
          'data' => $footer,
          'class' => 'footer',
        ]
        ];
        $table = [
                '#type' => 'table',
                '#header' => array(),
                '#rows' => $rows,
        ];
        $msg .= \Drupal::service('renderer')->render($table);
        $result=news_send_html_mail($msg, $subject,$to, $from,$cc='',$bcc='');
        if (!empty($result)) {
                \Drupal::messenger()->addError(t('There was a problem sending your message and it was not sent.').$result);
        }
        else {
                \Drupal::messenger()->addMessage(t('Subscription successfull for ') . $email);
        }
    }else {
 	$out = t('Your email address is not found in de list at ') . \Drupal::config('system.site')->get('name');
    }
    \Drupal::messenger()->addMessage($out);
    return $this->redirect('<front>');
  }

  public function news_unsubscribe($code) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $row = \Drupal::database()->query("SELECT email FROM {newsemail_current} where code=:code;", [
      ':code' => $code
      ])->fetchObject();
    if ($row->email) {
      $email = $row->email;
      $time = time();
      $entry['time'] = time();
      $entry['email'] = $email;
      $entry['code'] = $code;
      try {
        $id = \Drupal::database()->insert('newsemail_old')->fields($entry)->execute();
      }
      
        catch (Exception $e) {
        \Drupal::messenger()->addError(t('db_insert failed. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]));
        return;
      }
      try {
        \Drupal::database()->delete('newsemail_current')->condition('code', $code)->execute();
      }
      
        catch (Exception $e) {
        \Drupal::messenger()->addError(t('db_delete failed. Message = %message, query= %query', [
          '%message' => $e->getMessage(),
          '%query' => $e->query_string,
        ]));
        return;
      }
       $out = $email.t(' has been removed from the email news letter subscription list');

    }
    else {
	$out = $email.t(' Your email address has  not been found in de list');

    }
    \Drupal::messenger()->addMessage($out);
    return $this->redirect('news.news-admin-list');
  }

}
