<?php /**
 * @file
 * Contains \Drupal\news\Controller\DefaultController.
 */

namespace Drupal\news\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Core\Datetime;
use Drupal\Core\Entity;
use Drupal\Core\Render\Element; 
use Drupal\Core\Render\Markup;


/**
 * Default controller for the news module.
 */
class DefaultController extends ControllerBase {

  public function news_list() {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $options = ['absolute' => TRUE];
    $user = \Drupal::currentUser();
    $items = [];
    $num_rows = FALSE;
    if (\Drupal::currentUser()->hasPermission('news admin access')) {
	$out[] = [
        '#markup' => '<li>'.Link::fromTextAndUrl(t('Admin subscription'), Url::fromUri('internal:'.'/news/admin'))->toString(),
    ];
	$out[] = [
        '#markup' => '<li>'.Link::fromTextAndUrl(t('Admin settings'), Url::fromUri('internal:'.'/admin/config/user-interface/news'))->toString(),
    ];
    }
   $tablerows = [];
    if (\Drupal::currentUser()->hasPermission('access content')) {
	
	$list=array();
	$query = \Drupal::entityQuery('node')
  	->condition('status', 1) //published or not
  	->condition('type', 'news') //content type
  	->condition('langcode', $langcode) //content type
      	->sort('created', 'DESC')
  	->pager(15); //specify results to return
	$nids = $query->execute();
    	$list = array();
	$header = [t('News letters')];
	if (\Drupal::currentUser()->hasPermission('news admin access')) {
		$header[] = t('Send news');
		$header[] = t('Date send');
	}

	foreach ($nids as $nid) {
		$tablerow =[];
  		$node = \Drupal\node\Entity\Node::load($nid); 
		$node = $node->getTranslation($langcode);
		$news=Link::fromTextAndUrl($node->title->value,Url::fromUri(news_create_url_from_nid($nid,$langcode)))->toString();
		$tablerow[] =$news;
	        if (\Drupal::currentUser()->hasPermission('news admin access')) {
			$tablerow[]= Link::fromTextAndUrl(t('Zend de nieuwsbrief'), Url::fromUri('internal:'.'/news/admin/email/'.$nid))->toString();
          		$row = \Drupal::database()->query("SELECT time FROM {newsemail_send} where node=:node", [ ':node' => $nid ])->fetchObject();
          		if ($row) {
				$date=date_create();
				date_timestamp_set($date,$row->time);
				$tablerow[]= Link::fromTextAndUrl(date_format($date,"Y-m-d H:i:s"), Url::fromUri('internal:'.'/news/admin/emailsend/'.$nid))->toString();
				
          		}else{
				 $tablerow[]=t('Nog niet verzonden');
			}

        	}

		$tablerows[]=$tablerow;
	}  
    	$out[] = array(
        	'#theme' => 'table',
        	'#cache' => ['disabled' => TRUE],
        	'#header' => $header,
        	'#rows' => $tablerows,
        	'#attributes' => [
                	'class' => ['news-table'],
        	],
        );
    	$out[] =array('pager' => [
                '#type' => 'pager',
                ],
        );
    	return $out;
    }
  }

  public function newsemail_admin() {
	\Drupal::service('page_cache_kill_switch')->trigger();
	module_load_include('inc', 'news', 'news.email');
	$out=[];
	$out[] = [ '#markup' =>Link::fromTextAndUrl(t('List all subscribed email addresses '), Url::fromUri('internal:'.'/news/admin/list'))->toString()];
	
	$out[]= [ '#markup' =>"<p><hr><p>"];
	$out[] = \Drupal::formBuilder()->getForm('\Drupal\news\Form\SubscribeManual');	
      	return $out;
  }

  public function newsemail_list_current() {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $tablerows = [];
    $query = \Drupal::database()->select('newsemail_current', 'u');
    $query->fields('u', ['time','email','code']);
    $query->orderby('email');
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(15);
    $result = $pager->execute()->fetchAll();
    foreach ($result as $row) {
	$date=date_create();
	date_timestamp_set($date,$row->time);
	$tablerow = [
		$row->email,
		date_format($date,"Y-m-d H:i:s"),
		Link::fromTextAndUrl(t('Unsubscribe'), Url::fromUri('internal:'.'/news/unsubscribe/'.$row->code))->toString(),
	];
      $tablerows[] = $tablerow;
    }
    $header = ['Email address','Time/Date',''];
    $out[] = array(
  	'#theme' => 'table',
  	'#cache' => ['disabled' => TRUE],
  	'#header' => $header,
  	'#rows' => $tablerows,
	'#attributes' => [
    		'class' => ['news-table'],
  	],
	);
    $out[] =array('pager' => [
                '#type' => 'pager',
                ],
	);
    return $out;
  }
public function newsemail_send($node) {
    \Drupal::service('page_cache_kill_switch')->trigger();
    $tablerows = [];
    $result = \Drupal::database()->query("SELECT * FROM {newsemail_send} where node=:node", [
      ':node' => $node
      ]);
    if (!$result) {
      \Drupal::messenger()->addError(t('Error getting data from newsemail_send'));
    }
    foreach ($result as $row) {
      $date=date_create();
      date_timestamp_set($date,$row->time);
      $tablerow = [date_format($date,"Y-m-d H:i:s"), $row->subject, $row->emails];
      $tablerows[] = $tablerow;
    }
    $header = [t('Time'), t('Title'), t('Email addresses')];
    $out[] = array(
  	'#theme' => 'table',
  	'#cache' => ['disabled' => TRUE],
  	'#header' => $header,
  	'#rows' => $tablerows,
	'#attributes' => [
    		'class' => ['news-table'],
  	],
	);
    $out[] =array('pager' => [
                '#type' => 'pager',
                ],
	);

    return ($out);
  }


}
